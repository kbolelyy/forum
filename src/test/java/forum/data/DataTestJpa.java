package forum.data;

import forum.model.Comment;
import forum.model.Theme;
import forum.repository.CommentRepository;
import forum.repository.ThemeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import security.model.Role;
import security.model.User;
import security.repository.RoleRepository;
import security.repository.UserRepository;
import security.service.api.UserService;
import security.utils.SecurityConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;


@RunWith(SpringRunner.class)
@EnableJpaRepositories(basePackages = {"security.repository", "forum.repository"})
@ContextConfiguration(locations = {"classpath:config/data-base-config-test.xml"})
public class DataTestJpa {

        @Autowired
        UserService userService;

        @Autowired
        BCryptPasswordEncoder bCryptPasswordEncoder;

        @Autowired
        private UserRepository userRepository;

        @Autowired
        RoleRepository roleRepository;

        @Autowired
        ThemeRepository themeRepository;

        @Autowired
        CommentRepository commentRepository;




        @Test
        public void deleteComment(){

//                Optional<Theme> byId = themeRepository.findById((long) 2);
                commentRepository.deleteById((long) 3);
               /* Theme theme = byId.get();
                List<Comment> messages = theme.getMessages();
                messages.removeIf(comment -> comment.getId() ==  2);
                theme.setMessages(messages);
                themeRepository.save(theme);*/


        }


        @Test
        public void createTheme(){

                ArrayList<Theme> themes = new ArrayList<>();
                for (int i = 0; i <45 ; i++) {

                        Theme theme = new Theme();
                        theme.setHeader("Theme #" + i);
                        theme.setAuthor("admin");
                        theme.setCreated(new Date());
                        theme.setUpdated(new Date());
                        themes.add(theme);
                }

                themeRepository.saveAll(themes);

        }

        @Test
        public void createComment(){

              /*  Optional<Theme> byId = themeRepository.findById((long) 2);
                Theme theme = byId.get();
                Comment comment = new Comment();
                comment.setId((long) 60);
                comment.setAuthor("admin");
                comment.setMessage("HEY HOP LALA LAY");
                comment.setTheme(theme);

               commentRepository.save(comment);*/

             /*   Optional<Theme> byId = themeRepository.findById((long) 6);
                Theme theme = byId.get();

                Comment comment = new Comment();
                comment.setMessage("HOHOHOHOHOHO");
                comment.setAuthor("ASd");
                comment.setTheme(theme);
                commentRepository.save(comment);*/

                Optional<Theme> byId = themeRepository.findById((long) 1);
                Theme theme = byId.get();
                ArrayList<Comment> comments = new ArrayList<>();
                for (int i = 0; i < 40 ; i++) {
                        Comment comment = new Comment();
                        comment.setTheme(theme);
                        comment.setAuthor("admin");
                        comment.setUpdated(new Date());
                        comment.setCreated(new Date());
                        comment.setMessage("Just Message");
                        comments.add(comment);
                }

                theme.setMessages(comments);

                themeRepository.save(theme);


//                commentRepository.deleteById((long) 10);


        }

        @Test
        public void fillUser(){

                Role role = new Role();
                role.setRole(SecurityConstant.USER_ROLE);
                Role role2 = new Role();
                role2.setRole(SecurityConstant.ADMIN_ROLE);
                roleRepository.saveAll(Arrays.asList(role,role2));


                User user = new User();

                user.setFirstName("One");
                user.setSecondName("OneTwo");
                user.setPassword(bCryptPasswordEncoder.encode("Asssssss4!"));
                user.setLogin("user");
                user.setRoles(new HashSet<Role>(Arrays.asList(role)));


                User user2 = new User();

                user2.setFirstName("One");
                user2.setSecondName("OneTwo");
                user2.setPassword(bCryptPasswordEncoder.encode("Asssssss4!"));
                user2.setLogin("admin");
                user2.setRoles(new HashSet<>(Arrays.asList(role2)));

                userRepository.saveAll(Arrays.asList(user, user2));
        }
}
