package security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import security.model.Role;
import security.model.User;
import security.repository.RoleRepository;
import security.repository.UserRepository;
import security.service.api.UserService;
import security.utils.SecurityConstant;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User getUserById(Long id) {
        Optional<User> byId = userRepository.findById(id);
        return byId.get();
    }

    @Override
    public User getUserByLogin(String login) {
        return userRepository.getUserByLogin(login);
    }

    @Override
    public void saveUser(User user) {

        String encode = passwordEncoder.encode(user.getPassword());
        user.setPassword(encode);

        Set<Role> roles = new HashSet<>();
        Role role = roleRepository.findRoleByRole(SecurityConstant.USER_ROLE);
        roles.add(role);
        user.setRoles(roles);

        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

}
