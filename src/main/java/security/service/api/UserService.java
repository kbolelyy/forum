package security.service.api;

import security.model.User;

public interface UserService {

    User getUserById(Long id);

    User getUserByLogin(String login);

    void saveUser(User user);

    void deleteUser(Long id);

}
