package security.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Iterator;

public class SecurityUtils {

    public static String getCurrentPrincipalName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }


    public static boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Iterator<? extends GrantedAuthority> iterator = authentication.getAuthorities().iterator();
        boolean isAdmin = false;
        while (iterator.hasNext()) {
            GrantedAuthority next = iterator.next();
            isAdmin = next.getAuthority().equals(SecurityConstant.ADMIN_ROLE);
        }
        return authentication.isAuthenticated() && isAdmin;
    }
}
