package security.utils;

public interface SecurityConstant {

    String ADMIN_ROLE = "ADMIN_ROLE";
    String USER_ROLE = "USER_ROLE";

}
