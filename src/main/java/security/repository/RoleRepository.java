package security.repository;

import org.springframework.data.repository.CrudRepository;
import security.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findRoleByRole(String role);
}
