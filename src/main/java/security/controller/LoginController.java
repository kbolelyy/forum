package security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import security.model.Role;
import security.model.User;
import security.service.api.UserService;
import security.utils.SecurityConstant;
import utils.StringUtils;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/login", "/"}, method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value = "/authorization", method = RequestMethod.GET)
    public String authorizationPage(Model model) {
        model.addAttribute("user", new User());
        return "authorization";
    }

    @RequestMapping(value = "/authorization", method = RequestMethod.POST)
    public String authorization(@Valid User user, BindingResult bindingResult) {

        if (!isValidConfirmPassword(user)) {
            bindingResult.rejectValue("confirmPassword", "notmatch.password",
                    "Пароль и подтверждающий пароль не совпадают");
        }
        if (!isValidPassword(user.getPassword())){
            bindingResult.rejectValue("password", "error.password","ошибка пароля");
        }
        if (bindingResult.hasErrors()) {
            return "authorization";
        }

        userService.saveUser(user);

        return "login";
    }

    private boolean isValidPassword(String password) {
        Pattern p = Pattern.compile("^(?=.*\\d+.*)(?=.*[a-zA-Z]+.*)(?=.*[!@#$%]+.*)[0-9a-zA-Z!@#$%]{7,}$");
        Matcher m = p.matcher(password);
        return m.matches();
    }


    private boolean isValidConfirmPassword(User user) {
        if (!StringUtils.isNullOrEmpty(user.getConfirmPassword())) {
            return user.getConfirmPassword().equals(user.getPassword());
        }

        return false;
    }


}
