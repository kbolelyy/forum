package forum.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Theme implements Serializable {

    private static final long serialVersionUID = 4118068388889905216L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    @Size(min = 1, message = "Заголовок должен содержать текст.")
    private String header;

    @Column
    private String author;

    @Column
    @OneToMany(mappedBy = "theme", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> messages;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column
    private Date created;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column
    private Date updated;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<Comment> getMessages() {
        return messages;
    }

    public void setMessages(List<Comment> messages) {
        this.messages = messages;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theme theme = (Theme) o;
        return Objects.equals(id, theme.id) &&
                Objects.equals(header, theme.header);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, header);
    }

    @Override
    public String toString() {
        return "Theme{" +
                "id=" + id +
                ", header='" + header + '\'' +
                ", author='" + author + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
}
