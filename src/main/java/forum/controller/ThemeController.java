package forum.controller;

import forum.controller.constant.ThemeConstant;
import forum.model.Comment;
import forum.model.Theme;
import forum.repository.CommentRepository;
import forum.services.api.ThemeService;
import forum.services.exception.ThemeServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import security.utils.SecurityUtils;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/theme")
public class ThemeController {

    @Autowired
    private ThemeService themeService;

    @Autowired
    private CommentRepository commentRepository;


    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public String getTheme(@PathVariable Long id,
                           @RequestParam(value = "page", required = false) Integer pageNumber,
                           Model model) {

        if (id == null) {
            return "redirect:forum/";
        }

        Theme theme = themeService.getThemeById(id);

        Pageable pageRequest = PageRequest.of(pageNumber == null ? 0 : pageNumber,
                20, Sort.Direction.DESC, "updated");

        Page<Comment> commentsPage = commentRepository.getCommentsByThemeId(theme.getId(), pageRequest);
        commentsPage.getTotalPages();
        List<Comment> comments = commentsPage.getContent();

        Comment comment = new Comment();
        comment.setTheme(theme);

        model.addAttribute(ThemeConstant.THEME, theme);
        model.addAttribute(ThemeConstant.COMMENTS, comments);
        model.addAttribute(ThemeConstant.HASNEXT, commentsPage.hasNext());
        model.addAttribute(ThemeConstant.HASPREVIOUS, commentsPage.hasPrevious());
        model.addAttribute(ThemeConstant.TOTAL_PAGES, commentsPage.getTotalPages());
        model.addAttribute(ThemeConstant.PAGENUMBER, pageNumber);
        model.addAttribute(ThemeConstant.NEWCOMMENT, comment);
        model.addAttribute(ThemeConstant.NEWCOMMENT, comment);
        model.addAttribute("currentUser", SecurityUtils.getCurrentPrincipalName());
        model.addAttribute("isAdmin", SecurityUtils.isAdmin());

        return "theme";
    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    public String createTheme(@Valid Theme theme, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "redirect:/forum";
        }

        themeService.save(theme);
        return "redirect:" + theme.getId();
    }

    @RequestMapping(value = "/delete/{themeId}", method = RequestMethod.POST)
    public String deleteTheme(@PathVariable(value = "themeId") Long themeId) {
        themeService.delete(themeId);
        return "redirect:/forum/";
    }


    @RequestMapping(value = "/message/create/{themeId}", method = RequestMethod.POST)
    public String createMessage(@Valid Comment comment, @PathVariable(value = "themeId") Long themeId) throws ThemeServiceException {
        themeService.saveComment(comment, themeId);
        return "redirect:/theme/" + themeId;
    }

    @RequestMapping(value = "/message/delete", method = RequestMethod.POST)
    public String deleteMessage(@RequestParam Long themeId, @RequestParam Long commentId) {
        themeService.deleteComment(commentId);
        return "redirect:/theme/" + themeId;
    }


}
