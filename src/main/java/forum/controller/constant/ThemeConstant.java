package forum.controller.constant;

public interface ThemeConstant {

    String THEME = "theme";
    String THEMES = "themes";
    String COMMENTS = "comments";
    String NEWCOMMENT = "newComment";
    String HASNEXT = "hasNext";
    String HASPREVIOUS = "hasPrevious";
    String PAGENUMBER = "pageNumber";
    String TOTAL_PAGES = "totalPages";

}
