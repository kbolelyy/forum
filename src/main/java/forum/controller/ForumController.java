package forum.controller;

import forum.controller.constant.ThemeConstant;
import forum.model.Theme;
import forum.repository.ThemeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import security.utils.SecurityUtils;

@Controller
@RequestMapping(value = "/forum")
public class ForumController {

    @Autowired
    ThemeRepository repository;

    @RequestMapping(method = RequestMethod.GET)
    public String forumPage(Model model, @RequestParam(value = "page", required = false) Integer pageNumber) {

        Pageable pageRequest = PageRequest.of(pageNumber == null ? 0 : pageNumber,
                20, Sort.Direction.DESC, "updated");

        Page<Theme> themePage = repository.findAll(pageRequest);

        model.addAttribute(ThemeConstant.THEME, new Theme());
        model.addAttribute(ThemeConstant.THEMES, themePage.getContent());
        model.addAttribute(ThemeConstant.PAGENUMBER, pageNumber);
        model.addAttribute(ThemeConstant.TOTAL_PAGES, themePage.getTotalPages());
        model.addAttribute(ThemeConstant.HASPREVIOUS, themePage.hasPrevious());
        model.addAttribute(ThemeConstant.HASNEXT, themePage.hasNext());
        model.addAttribute("currentUser", SecurityUtils.getCurrentPrincipalName());
        model.addAttribute("isAdmin", SecurityUtils.isAdmin());
        return "forum";
    }

}
