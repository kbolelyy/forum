package forum.services.impl;

import forum.model.Comment;
import forum.model.Theme;
import forum.repository.CommentRepository;
import forum.repository.ThemeRepository;
import forum.services.api.ThemeService;
import forum.services.exception.ThemeServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import security.utils.SecurityUtils;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class ThemeServiceImpl implements ThemeService {

    @Autowired
    private ThemeRepository themeRepository;

    @Autowired
    private CommentRepository commentRepository;


    @Override
    public Theme getThemeById(Long id) {
        return themeRepository.getById(id);
    }

    @Override
    public void save(Theme theme) {
        theme.setAuthor(SecurityUtils.getCurrentPrincipalName());
        theme.setCreated(new Date());
        theme.setUpdated(new Date());
        themeRepository.save(theme);
    }

    @Override
    public void delete(Long id) {
        themeRepository.deleteById(id);
    }

    @Override
    public void saveComment(Comment comment, Long id) throws ThemeServiceException {

        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if (optionalTheme.isPresent()) {

            Theme theme = optionalTheme.get();
            theme.setUpdated(new Date());

            comment.setAuthor(SecurityUtils.getCurrentPrincipalName());
            comment.setCreated(new Date());
            comment.setUpdated(new Date());
            comment.setTheme(theme);

            theme.getMessages().add(comment);


            themeRepository.save(theme);

        } else {
            throw new ThemeServiceException(MessageFormat.format("Не удается сохранить комментарий для темы: {}", id));
        }
    }


    @Override
    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }
}
