package forum.services.exception;

public class ThemeServiceException extends Exception {

    private static final long serialVersionUID = -2193191204058417757L;

    public ThemeServiceException(String message) {
        super(message);
    }
}
