package forum.services.api;

import forum.model.Comment;
import forum.model.Theme;
import forum.services.exception.ThemeServiceException;

public interface ThemeService {

    Theme getThemeById(Long id);

    void save(Theme theme);

    void delete(Long id);

    void saveComment(Comment comment, Long id) throws ThemeServiceException;

    void deleteComment(Long id);

}
