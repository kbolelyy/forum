<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="hasPrevious" required="true"
              type="java.lang.Boolean"%>
<%@ attribute name="hasNext" required="true"
              type="java.lang.Boolean"%>
<%@ attribute name="pageNumber" required="true" type="java.lang.Integer"%>
<%@ attribute name="theme" required="true" type="forum.model.Theme"%>
  <ul>
        <c:choose>
            <c:when test="${hasPrevious}"><li><a href="/theme/${theme.id}?page=${pageNumber - 1}"></a>previous</li></c:when>
            <c:when test="${!hasPrevious}"><li style="pointer-events: none;">previous</li></c:when>
        </c:choose>
    <c:forEach begin="${pageNumber}" end="${pageNumber + 2}" step="1" var="page">
        <li><a href="/theme/${theme.id}?page=${page}">${page}</a></li>
    </c:forEach>
        <c:choose>
            <c:when test="${hasNext}"><li><a href="/theme/${theme.id}?page=${pageNumber + 3}">next</a></li></c:when>
            <c:when test="${!hasNext}"><li style="pointer-events: none;">previous</li></c:when>
        </c:choose>
    </ul>