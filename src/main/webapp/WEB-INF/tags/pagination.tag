<%@ tag language="java" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="hasPrevious" required="true"
              type="java.lang.Boolean" %>
<%@ attribute name="hasNext" required="true"
              type="java.lang.Boolean" %>
<%@ attribute name="pageNumber" required="true" type="java.lang.Long" %>
<%@ attribute name="totalPages" required="true" type="java.lang.Long" %>
<%@ attribute name="hrefPath" required="true" type="java.lang.String" %>

<c:set var="D" value="awd"/>


<nav>
    <ul class="pagination">
        <c:choose>
            <c:when test="${hasPrevious}">
                <li class="page-item">
                    <a class="page-link" href="${hrefPath}?page=${pageNumber - 1}">Previous</a>
                </li>
            </c:when>
            <c:when test="${!hasPrevious}">
                <li class="page-item disabled">
                    <a class="page-link">Previous</a>
                </li>
            </c:when>
        </c:choose>
        <c:forEach begin="${pageNumber == null ? 0 : pageNumber}" end="${(totalPages % 3 > pageNumber) ? 0 : totalPages}" step="1" var="page">
            <c:choose>
                <c:when test="${pageNumber == null ? 0 == page : pageNumber == page}">
                    <li class="page-item active" aria-current="page">
                <span class="page-link">${pageNumber + 1}
                <span class="sr-only">(current)</span>
                </span>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link" href="${hrefPath}?page=${page}">${page + 1}</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:choose>
            <c:when test="${hasNext}">
                <li class="page-item">
                    <a class="page-link" href="${hrefPath}?page=${pageNumber + 1}">Next</a>
                </li>
            </c:when>
            <c:when test="${!hasNext}">
                <li class="page-item disabled">
                    <a class="page-link disabled">Next</a>
                </li>
            </c:when>
        </c:choose>
    </ul>
</nav>