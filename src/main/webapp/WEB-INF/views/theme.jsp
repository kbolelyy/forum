<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 02.06.2019
  Time: 21:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${theme.header}</title>
</head>
<link rel="stylesheet" type="text/css" href="WEB-INF/css/main.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<body>
<jsp:include page="toolbar.jsp"/>

<table style="margin: 30px">
    <%--Header theme--%>
    <h2 style="margin: 30px">${theme.header}</h2>

    <%--Button back to list theme--%>
    <button style="margin-left: 10px" type="button" onclick="location.href='${pageContext.request.contextPath}/forum'"
            name="submit" class="btn btn-primary">Назад к списку тем</button>

    <%--Comments--%>
    <c:forEach var="comment" items="${comments}">
        <table class="table" style="margin: 15px;margin-left: 50px; width: 70%">
            <tbody>
            <tr>
                <td style="margin: 5px; padding: 5px; width: 18%; text-align: left;">
                    <div style="margin: 5px;font-weight: bold">${comment.author}</div>
                    <div>Дата: ${comment.updated}</div>
                </td>
                <td style="margin-left: 20px; padding: 5px; width: 50%;text-align: justify">
                    <div>${comment.message}</div>
                </td>
                <td>
                    <c:if test="${(isAdmin eq true) || (currentUser eq comment.author)}">
                        <form:form action="${pageContext.request.contextPath}/theme/message/delete?themeId=${theme.id}&commentId=${comment.id}">
                            <button type="submit" class="btn btn-primary">Удалить</button>
                        </form:form>
                    </c:if>
                </td>
            </tr>
            </tbody>

        </table>
    </c:forEach>
    <%--Comments--%>

    <%--Input field message--%>
    <form:form cssStyle="width: 500px; margin: 30px" method="post" modelAttribute="newComment"
               action="${pageContext.request.contextPath}/theme/message/create/${theme.id}">
        <div style="margin: 10px;width: 500px" class="form-group">
            <label for="msg">Написать комментарий:</label>
            <form:textarea class="form-control" id="msg" name="message" path="message"/>
            <form:errors path="message" cssClass="error"/>
            <button style="margin-top: 10px" type="submit" class="btn btn-primary">Отправить</button>
        </div>
    </form:form>
    <%--Input field message--%>



    <%--Pagination--%>
    <div style="margin: 30px; margin-left: 10px">
        <tags:pagination hasNext="${hasNext}" hasPrevious="${hasPrevious}" totalPages="${t}"
                         pageNumber="${pageNumber}" hrefPath="${pageContext.request.contextPath}/theme/${theme.id}"/>
    </div>

    <%--Pagination--%>

</table>
</body>

</html>
