<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 02.06.2019
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Форум</title>
</head>
    <jsp:include page="toolbar.jsp"/>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<body>

<!-- Button trigger modal -->
<button type="button" style="margin: 30px; margin-left: 50px" class="btn btn-primary" data-toggle="modal"
        data-target="#exampleModalLong">Создать тему
</button>
<!-- Modal -->

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form:form id="modalTheme" modelAttribute="theme"
                   action="${pageContext.request.contextPath}/theme/create">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Введите название темы</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <form:input path="header" type="text" class="form-control" placeholder="Заголовок темы"/>
                        <form:errors path="header" cssClass="alert alert-danger"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form:form>
    </div>
</div>


<%--Themes--%>
<table style="width: 90%; margin-left: 50px" class="table table-hover">
    <thead class="thead-dark">
    <tr>
        <th>Тема</th>
        <th>Автор</th>
        <th>Обновлено</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="theme" items="${themes}">
        <tr>
            <td>
                <h4><a href="${pageContext.request.contextPath}/theme/${theme.id}">${theme.header}</a></h4>
            </td>
            <td>
                <div>${theme.author}</div>
            </td>
            <td>
                <div>${theme.updated}</div>
            </td>
            <td>
                <c:if test="${isAdmin eq true}">
                    <form:form action="${pageContext.request.contextPath}/theme/delete/${theme.id}">
                        <button type="submit" class="btn btn-primary">Удалить</button>
                    </form:form>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<%--Themes--%>

<%--Pagination--%>

    <div style="margin: 50px">
        <tags:pagination pageNumber="${pageNumber}" hasPrevious="${hasPrevious}" totalPages="${totalPages}"
                         hasNext="${hasNext}" hrefPath="${pageContext.request.contextPath}/forum"/>
    </div>

<%--Pagination--%>



</body>
</html>
