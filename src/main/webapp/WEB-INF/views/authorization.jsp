<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Авторизация</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<body>
<div>
    <form:form method="post" modelAttribute="user" action="${pageContext.request.contextPath}/authorization"
               cssStyle="width: 300px;margin: auto;margin-top: 150px;">
        <div class="form-group">
            <div>
                <label for="login">Логин</label>
                <form:input type="text" class="form-control" id="login" placeholder="Логин" path="login"/><br/>
                <form:errors path="login" cssClass="alert alert-danger"/>
            </div>
            <div>
                <label for="userName">Имя</label>
                <form:input type="text" class="form-control" id="userName" placeholder="Имя" path="firstName"/><br/>
                <form:errors path="login" cssClass="alert alert-danger"/>
            </div>
            <div>
                <label for="secondName">Фамилия</label>
                <form:input id="secondName" class="form-control" type="text" placeholder="Фамилия"
                            path="secondName"/><br/>
                <form:errors path="login" cssClass="alert alert-danger"/>
            </div>
            <div>
                <label for="password">Пароль</label>
                <form:input type="password" class="form-control" id="password" placeholder="Пароль"
                            path="password"/><br/>
                <form:errors path="login" cssClass="alert alert-danger"/>
            </div>
            <div>
                <label for="confirmPassword">Подтверждение</label>
                <form:input type="password" class="form-control" id="confirmPassword" placeholder="Подтвердите пароль"
                            path="confirmPassword"/><br/>
                <form:errors path="login" cssClass="error"/>
            </div>
        </div>
        <div style="text-align: center">
            <button type="submit" class="btn btn-primary">Регистрация</button>
            <button type="button" onclick="location.href='${pageContext.request.contextPath}/login'"
                    class="btn btn-primary">Назад
            </button>
        </div>

    </form:form>
</div>
</body>
</html>

<%--Регистрация
На данной странице находятся поля ввода логина, имени, фамилии, пароля, подтверждение пароля и кнопка регистрация.
Необходимо проверять поле логина на уникальность, для пароля должна быть проверка на кол-во символов (не менее 8)
на присутствие хотя бы одной цифры, строчной буквы, заглавной буквы и символа (из !@#$%). Asssssss4! Bdddddddd5@
В случае корректности всех заполненных полей, происходит регистрация нового пользователя и переход на страницу логина.
--%>