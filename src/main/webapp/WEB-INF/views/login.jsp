<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Логин</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="WEB-INF/css/login.css">
<body>
<div style="width: 300px;margin: auto;margin-top: 200px;">
    <form class="form-signin" method="post" action="${pageContext.request.contextPath}/login">
        <h1 class="h3 mb-3 font-weight-normal">Логин</h1>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div class="form-group">
            <label class="sr-only" for="login">Логин</label>
            <input type="text" class="form-control" name="username" id="login" placeholder="Логин">
        </div>
        <div class="form-group">
            <label class="sr-only" for="password">Пароль</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
        </div>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" id="remember_me" name="_spring_security_remember_me">
                Запомнить меня
            </label>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Логин</button>
        <button type="button" onclick="location.href='${pageContext.request.contextPath}/authorization'"
                name="submit" class="btn btn-primary">
            Регистрация
        </button>

        <p style="text-align: center" class="mt-5 mb-3 text-muted">© 2019</p>
    </form>
</div>
</body>
</html>

